// == Module imports
var
	gulp = require('gulp'),

	// general propouse
	sourcemaps = require('gulp-sourcemaps'),
	del = require('del'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),

	// styles
	less = require('gulp-less'),
	cssmin = require('gulp-cssmin'),

	// scripts
	typescript = require('gulp-typescript'),
	uglify = require('gulp-uglify'),

	// testing an dev
	karma = require('karma').server,
	connect = require('gulp-connect'),

	// scaffolding
	template = require('gulp-template'),
	yarg = require('yargs');


// == Variable Definitions

var
	appDistRootPath = "wwwroot/",
	lessSources = [
		'app/shared/styles/app.less'
	],
	rootAssetsSources = [
		'app/assets/html/*.*',
		'app/assets/ico/*.*',
		'app/assets/txt/*.*',
	],
	tmplSources = [
		'app/**/*.tmpl.html',
	],
	imgAssetsSources = [
		'app/assets/img/*.png',
		'app/assets/img/*.jpg',
		'app/assets/img/*.gif',
		'app/assets/img/*.webp',
	],
	fontAssetsSources = [
		'bower_components/font-awesome/fonts/*.*'
	],
	jsLibSources = [
		'bower_components/lodash/lodash.js',
		'bower_components/angular/angular.js',
		'bower_components/angular-animate/angular-animate.js',
		'bower_components/angular-messages/angular-messages.js',
		'bower_components/angular-dpd/index.js',
		'bower_components/socket.io-client/socket.io.js',
		'bower_components/angular-ui-router/release/angular-ui-router.js',
		'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
		'bower_components/angular-md5/angular-md5.js'
	],
	tsAppSources = [
		'app/**/*.ts',
		'!app/**/*Specs.ts',
	],
	tsAppSpecsSources = [
		'app/**/*Specs.ts',
	],
	jsAppTargetMinFileName = 'js/app.min.js',
	jsAppSpecsTargetFileName = 'appSpecs.js',
	jsLibTargetFileName = 'js/libs.js';

// == FUNCTIONS USED BY TASKS

function cleanDir(dirpath, done) {
	del(dirpath, done);
}

function copyAssets(sources, dir) {
	return gulp
		.src(sources)
		.pipe(gulp.dest(dir))
		.pipe(connect.reload());
}

function bundleJS(sources, destination, filename) {
	return gulp
		.src(sources)
		.pipe(sourcemaps.init())
		.pipe(concat(filename))
		.pipe(uglify())
		.pipe(sourcemaps.write("."))
		.pipe(gulp.dest(destination))
		.pipe(connect.reload());
}

function bundleApp() {
	var tsResult = gulp.src(tsAppSources)
		.pipe(sourcemaps.init())
		.pipe(typescript({
			sortOutput: true
		}));

	return tsResult.js
		.pipe(concat(jsAppTargetMinFileName))
		.pipe(uglify())
		.pipe(sourcemaps.write("."))
		.pipe(gulp.dest(appDistRootPath))
		.pipe(connect.reload());
}

function bundleSpecs() {
	var tsResult = gulp.src(tsAppSpecsSources)
		.pipe(typescript({
			sortOutput: true
		}));

	return tsResult.js
		.pipe(concat(jsAppSpecsTargetFileName))
		.pipe(gulp.dest("."));
}

function buildStyles() {
	return gulp
		.src(lessSources)
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(cssmin())
		.pipe(sourcemaps.write("."))
		.pipe(gulp.dest(appDistRootPath + 'css'))
		.pipe(connect.reload());
}

function processSkaffold(job) {
	gulp.src('skaffold/' + job.templatePath)
		.pipe(template(job.templateValues))
		.pipe(rename(job.destinationFileName))
		.pipe(gulp.dest(job.destinationFolder));
}

// == SCAFFOLDS
gulp.task('ska-controller', function() {
	var argv = yarg
		.alias('n', 'name')
		.alias('u', 'url')
		.alias('m', 'module')
		.require('name', 'parameter name (-n) is required')
		.require('module', 'parameter module (-m) is required')
		.require('url', 'parameter url (-u) is required')
		.argv;
	argv.Name = argv.name[0].toUpperCase() + argv.name.slice(1);


	argv.route = 'app.' + argv.module + '.' + argv.name;
	argv.namespace = 'app.' + argv.module;
	var path = 'app/' + argv.module;

	if (argv.module == 'app') {
		path = 'app/shared';
		argv.route = 'app';
		argv.namespace = 'app';
	}

	processSkaffold({
		templatePath: 'controller/controller.ts.tmpl',
		templateValues: argv,
		destinationFileName: argv.name + 'Ctrl.ts',
		destinationFolder: path + '/controllers'
	});
	processSkaffold({
		templatePath: 'controller/view.html.tmpl',
		templateValues: argv,
		destinationFileName: argv.name + 'Ctrl.tmpl.html',
		destinationFolder: path + '/controllers'
	});
	processSkaffold({
		templatePath: 'controller/spec.ts.tmpl',
		templateValues: argv,
		destinationFileName: argv.name + 'CtrlSpecs.ts',
		destinationFolder: path + '/controllers'
	});

});

gulp.task('ska-service', function() {
	var argv = yarg
		.alias('n', 'name')
		.alias('m', 'module')
		.require('name', 'parameter name (-n) is required')
		.require('module', 'parameter module (-m) is required')
		.argv;
	argv.Name = argv.name[0].toUpperCase() + argv.name.slice(1);


	argv.route = 'app.' + argv.module + '.' + argv.name;
	argv.namespace = 'app.' + argv.module;
	var path = 'app/' + argv.module;

	if (argv.module == 'app') {
		path = 'app/shared';
		argv.route = 'app';
		argv.namespace = 'app';
	}

	processSkaffold({
		templatePath: 'service/service.ts.tmpl',
		templateValues: argv,
		destinationFileName: argv.name + 'Fac.ts',
		destinationFolder: path + '/services'
	});
	processSkaffold({
		templatePath: 'service/spec.ts.tmpl',
		templateValues: argv,
		destinationFileName: argv.name + 'FacSpecs.ts',
		destinationFolder: path + '/services'
	});

});
gulp.task('ska-directive', function() {
	var argv = yarg
		.alias('n', 'name')
		.alias('t', 'tagName')
		.alias('m', 'module')
		.require('name', 'parameter name (-n) is required')
		.require('module', 'parameter module (-m) is required')
		.require('tagName', 'parameter tagName (-t) is required')
		.argv;
	argv.Name = argv.name[0].toUpperCase() + argv.name.slice(1);


	argv.namespace = 'app.' + argv.module;
	var path = 'app/' + argv.module;

	if (argv.module == 'app') {
		path = 'app/shared';
		argv.namespace = 'app';
	}

	processSkaffold({
		templatePath: 'directive/directive.ts.tmpl',
		templateValues: argv,
		destinationFileName: argv.name + 'Directive.ts',
		destinationFolder: path + '/directives'
	});
	processSkaffold({
		templatePath: 'directive/view.html.tmpl',
		templateValues: argv,
		destinationFileName: argv.name + 'Directive.tmpl.html',
		destinationFolder: path + '/directives'
	});
	processSkaffold({
		templatePath: 'directive/spec.ts.tmpl',
		templateValues: argv,
		destinationFileName: argv.name + 'DirectiveSpecs.ts',
		destinationFolder: path + '/directives'
	});

});

// == TASK DEFINITIONS
gulp.task('clean-all', function(done) {
	cleanDir('appSpecs.js');
	cleanDir('wwwroot/**/*', done);
});
gulp.task('clean-root-assets', function(done) {
	cleanDir('wwwroot/*.*', done);
});
gulp.task('clean-tmpl-assets', function(done) {
	cleanDir('wwwroot/templates/**/*', done);
});
gulp.task('clean-img-assets', function(done) {
	cleanDir('wwwroot/img/**/*', done);
});
gulp.task('clean-font-assets', function(done) {
	cleanDir('wwwroot/fonts/**/*', done);
});

gulp.task('clean-libs', function(done) {
	cleanDir(jsLibTargetFileName, done);
});
gulp.task('clean-app', function(done) {
	cleanDir(jsAppTargetMinFileName, done);
});
gulp.task('clean-styles', function(done) {
	cleanDir('wwwroot/css/**/*', done);
});

gulp.task('copy-root-assets', ['clean-root-assets'], function(done) {
	return copyAssets(rootAssetsSources, appDistRootPath);
});

gulp.task('copy-tmpl-assets', ['clean-tmpl-assets'], function() {
	return copyAssets(tmplSources, appDistRootPath + 'templates');
});

gulp.task('copy-img-assets', ['clean-img-assets'], function() {
	return copyAssets(imgAssetsSources, appDistRootPath + 'img');
});

gulp.task('copy-font-assets', ['clean-font-assets'], function() {
	return copyAssets(fontAssetsSources, appDistRootPath + 'fonts');
});
gulp.task('build-libs', function() {
	return bundleJS(jsLibSources, appDistRootPath, jsLibTargetFileName);
});
gulp.task('rebuild-libs', ['clean-libs'], function() {
	return bundleJS(jsLibSources, appDistRootPath, jsLibTargetFileName);
});

gulp.task('build-app', function() {
	return bundleApp();
});

gulp.task('rebuild-app', ['clean-app'], function() {
	return bundleApp();
});

gulp.task('rebuild-specs', function() {
	return bundleSpecs();
});

gulp.task('build-specs', function() {
	return bundleSpecs();
});

gulp.task('rebuild-styles', ['clean-styles'], function() {
	return buildStyles();
});

gulp.task('build-styles', function() {
	return buildStyles();
});

gulp.task('connect', ['build'], function() {
	return connect.server({
		root: 'wwwroot',
		livereload: true
	});
});

// == MACRO TASKS DEFINITIONS

gulp.task('copy-assets', [
	'copy-root-assets',
	'copy-tmpl-assets',
	'copy-font-assets',
	'copy-img-assets'
]);

gulp.task('build', [
	'copy-assets',
	'build-libs',
	'build-app',
	'build-styles'
]);

gulp.task('rebuild', [
	'copy-assets',
	'rebuild-libs',
	'rebuild-app',
	'rebuild-styles'
]);

gulp.task('tdd', ['build-specs'], function(done) {
	karma.start({
		configFile: __dirname + '/karma.conf.js'
	}, done);
});

gulp.task('watch', ['build'], function() {
	gulp.watch(rootAssetsSources, ['copy-root-assets']);
	gulp.watch(tmplSources, ['copy-tmpl-assets']);
	gulp.watch('app/**/*.less', ['build-styles']);
	gulp.watch(tsAppSources, ['build-app']);
	gulp.watch(jsLibSources, ['build-libs']);
	gulp.watch(tsAppSpecsSources, ['build-specs']);
});

gulp.task('dev', [
	'build',
	'tdd',
	'connect',
	'watch'
]);
