/// <reference path='shared/typings/controllerAnnotation'/>
/// <reference path='./shared/services/authPrincipalFac'/>
/// <reference path='./shared/services/authFac'/>
/// <reference path='appModule'/>

module app {
  export class AppController {
    static annotations: app.shared.typings.IContorllerAnotation = {
      routing: {
        name:'app',
        path:'',
        title:'Inicio',
        public:true
      },
      controller: {
        id:'app.appCtrl',
        templateUrl:'templates/app.tmpl.html',
      }
    }
    static $inject = [];

    constructor() {

    }
  }
  app.registerController(appModule, AppController);


  /*
  appModule.run([
    '$rootScope',
    '$state',
    '$stateParams',
    'app.authFactory',
    'app.authPrincipalFactory',
    ($rootScope, $state, $stateParams, auth: app.AuthFactoryClass, principal: app.AuthPrincipalFactoryClass)=> {
      $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
          // track the state the user wants to go to; authorization service needs this
          if(toState.name==='app.login') return;
          console.log('calling authorization from stateChangeStart on '+toState.name)
          $rootScope.toState = toState;
          $rootScope.toStateParams = toStateParams;
          // if the principal is resolved, do an authorization check immediately. otherwise,
          // it'll be done when the state it resolved.
          if (principal.isIdentityResolved()) return auth.authorize();
        });
  }]);*/
}
