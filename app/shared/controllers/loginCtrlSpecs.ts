/// <reference path='../../../typings/angularjs/angular.d.ts'/>
/// <reference path='../../../typings/angular-ui-router/angular-ui-router.d.ts'/>
/// <reference path='../../../typings/angularjs/angular-mocks.d.ts'/>
/// <reference path='../../../typings/mocha/mocha.d.ts'/>
/// <reference path='../../../typings/chai/chai.d.ts'/>
/// <reference path='../../../typings/sinon/sinon.d.ts'/>
/// <reference path='../../../typings/q/Q.d.ts'/>
/// <reference path='../typings/navState.d.ts'/>
/// <reference path='../../appModule'/>
/// <reference path='./loginCtrl'/>

describe('app',()=>{
  var expect = chai.expect;
  describe('loginController', ()=> {

    var __dpdFake: any;
    var __scopeFake: any;

    function createController() {
      var d = Q.defer();
      __dpdFake = {
        people: {
          exec: (param1, param2) => {

          }
        }
      }

      return new app.LoginController(__dpdFake, __scopeFake);
    }


    it('controller should be defined', ()=> {
      expect(app.LoginController).not.to.be.null.and.not.to.be.undefined;
    });

    it('this is the failing test',()=>{
      expect(true).to.be.false;
    });

  });
});
