/// <reference path="../../appModule"/>
module app {
  export class LoginController {
    static annotations: app.shared.typings.IContorllerAnotation = {
      routing: {
        name:'app.login',
        path:'/login',
        title:'Login',
        showOnNav: false,
        public:true
        //abstract: true
      },
      controller: {
        id:'app.LoginController',
        templateUrl:'templates/shared/controllers/loginCtrl.tmpl.html',
      }
    }
    static $inject = ['dpd', '$rootScope'];

    constructor(private dpd,private $rootScope) {

    }

    doLogin(username, password) {
      this.dpd.people.exec('login',{username: username, password: password})
        .then((user)=>{
          this.$rootScope.$broadcast('login:currentUserChanged', user);
        },(err)=>{
          console.error(err);
        });
    }
  }
  app.registerController(appModule, LoginController);
}
