/// <reference path="../../appModule"/>
/// <reference path="../../../typings/angularjs/angular.d.ts"/>

module app {
  export class UserWidgetDirectiveController
  {
    static directiveId = 'dpdUserWidget'
    static ddo : ng.IDirective = {
      restrict: 'E',
        templateUrl: 'templates/shared/directives/userWidgetDirective.tmpl.html',
        scope: {

        },
        controllerAs: 'vm',
        bindToController: true,
        replace:true
    }

    currentUser:any;
    loading:boolean;

    static $inject = ['dpd'];
    constructor(private dpd) {
      this.loading = true;
      dpd.people.get("me").then((result)=>{
        this.currentUser = result.data;
        this.loading=false;
      },(err)=> {
        this.loading=false;
        console.error('error loading active user',err);
      });
    }

    logOut() {
      this.dpd.people.exec('logout')
        .then(()=>{
          this.currentUser=null;
        }, (err)=> {
          console.log(err);
        });
    }
  }

  app.registerDirective(appModule, UserWidgetDirectiveController);
}
