/// <reference path='../../../typings/angularjs/angular.d.ts'/>
/// <reference path='../../../typings/angular-ui-router/angular-ui-router.d.ts'/>
/// <reference path='../../../typings/angularjs/angular-mocks.d.ts'/>
/// <reference path='../../../typings/mocha/mocha.d.ts'/>
/// <reference path='../../../typings/chai/chai.d.ts'/>
/// <reference path='../../../typings/sinon/sinon.d.ts'/>
/// <reference path='../typings/navState.d.ts'/>
/// <reference path='../../appModule'/>
/// <reference path='./navDirective'/>

describe('app',()=>{
  var expect = chai.expect;
  describe('navDirective', ()=> {

    function createController(states?, isDenyed?) {
      if(!states)
      {
        states =[];
      }
      var $states = <ng.ui.IStateService>{
        get:()=> {
          return states;
        }
      };
      var principal_allowed= {
        isInAnyRole: (role)=> {
          return true;
        }
      };
      var principal_denyed= {
        isInAnyRole: (role)=> {
          return false;
        }
      };

      var principal = <app.AuthPrincipalFactoryClass>principal_allowed;
      if(isDenyed) {
        principal=<app.AuthPrincipalFactoryClass>principal_denyed;
      }
      return new app.NavDirectiveController($states, principal);
    }


    it('directive should be defined', ()=> {
      expect(app.NavDirectiveController).not.to.be.null.and.not.to.be.undefined;
    });

    it('sets correctly a root scope when present',()=>{

      var states = [
        { name: 'app', navData: {title:'Test'}}
      ]
      var testController = createController(states);

      expect(testController.rootState.navData.title).to.equal('Test');
    });
    it('throws an eror when no root scope is present',()=>{

      var states = [
        { name: 'app.something', navData: {title:'Test 1'}},
        { name: 'app.otherthing', navData: {title:'Test 2'}},
      ]
      var fn = ()=> {
        createController(states);
      };
      expect(fn).to.throw(/root state was not provided/);
    });

    describe('public routes',()=> {
      it('sets correctly a level one state when present',()=>{

        var states = [
          { name: 'app', navData: {title:'Test'}, data: {public:true}},
          { name: 'app.something', navData: {title:'Test 1'}, data: {public:true}},
          { name: 'app.otherthing', navData: {title:'Test 2'}, data: {public:true}},
        ];
        var testController = createController(states);

        expect(testController.levelOneStates.length).to.equal(2);
      });

      it('when a level one state has showOnNav=false, dont count it as child',()=>{

        var states = [
          { name: 'app', navData: {title:'Test'}, data: {public:true}},
          { name: 'app.something', navData: {title:'Test 1'}, data: {public:true}},
          { name: 'app.otherthing', navData: {title:'Test 2', showOnNav:false}, data: {public:true} },
        ];
        var testController = createController(states);

        expect(testController.levelOneStates.length).to.equal(1);
        expect(testController.levelOneStates[0].name).to.equal('app.something');
      });

      it('sets correctly a level two state when present',()=>{

        var states = [
          { name: 'app', navData: {title:'Test'}, data: {public:true}},
          { name: 'app.otherthing', navData: {title:'Test 2'}, abstract:true, data: {public:true}},
          { name: 'app.otherthing.child', navData: {title:'Test 3'}, url:'state', data: {public:true}},
        ];
        var testController = createController(states);
        var levelOneState = testController.levelOneStates[0];

        expect(levelOneState.navData.childStates).not.to.be.undefined

      });
      it('when a level two state has parameters, dont count it as child',()=>{

        var states = [
          { name: 'app', navData: {title:'Test'}, data: {public:true}},
          { name: 'app.otherthing', navData: {title:'Test 2'}, abstract:true, data: {public:true}},
          { name: 'app.otherthing.child', navData: {title:'Test 3'}, url:'state/:id', data: {public:true}},
        ];
        var testController = createController(states);
        var levelOneState = testController.levelOneStates[0];

        expect(levelOneState.navData.childStates).to.be.empty

      });
      it('when a level two state is listed as showOnNav=false, dont count it as child',()=>{

        var states = [
          { name: 'app', navData: {title:'Test'}, data: {public:true}},
          { name: 'app.otherthing', navData: {title:'Test 2'}, abstract:true, data: {public:true}},
          { name: 'app.otherthing.child', navData: {title:'Test 3', showOnNav:false}, url:'state', data: {public:true}},
        ];
        var testController = createController(states);
        var levelOneState = testController.levelOneStates[0];

        expect(levelOneState.navData.childStates).to.be.empty

      });
    });

    describe('private routes',()=> {

      describe('not alowed', ()=> {
          it('does not includes level one state when present',()=>{

          var states = [
            { name: 'app', navData: {title:'Test'}, data: {public:true, roles:['whatever']}},
            { name: 'app.something', navData: {title:'Test 1'}, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing', navData: {title:'Test 2'}, data: {public:false, roles:['whatever']}},
          ];
          var testController = createController(states, true);

          expect(testController.levelOneStates.length).to.equal(0);
        });
         it('does not includes a level two state when only the level two is private and not alowed',()=>{

          var states = [
            { name: 'app', navData: {title:'Test'}, data: {public:true}},
            { name: 'app.otherthing', navData: {title:'Test 2'}, abstract:true, data: {public:true}},
            { name: 'app.otherthing.child', navData: {title:'Test 3'}, url:'state', data: {public:false, roles:['whatever']}},
          ];
          var testController = createController(states,true);
          var levelOneState = testController.levelOneStates[0];

          expect(levelOneState.navData.childStates).to.has.length(0);

        });
         it('does not includes a level two state when the parent state is private and not alowed',()=>{

          var states = [
            { name: 'app', navData: {title:'Test'}, data: {public:true}},
            { name: 'app.something', navData: {title:'Test 2'}, abstract:true, data: {public:true}},
            { name: 'app.otherthing', navData: {title:'Test 2'}, abstract:true, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing.child', navData: {title:'Test 3'}, url:'state', data: {public:false, roles:['whatever']}},
          ];
          var testController = createController(states,true);
          

          expect(testController.levelOneStates.length).to.equal(1);

        });
      });
      describe('alowed', ()=> {
        it('sets correctly a level one state when present',()=>{

          var states = [
            { name: 'app', navData: {title:'Test'}, data: {public:false, roles:['whatever']}},
            { name: 'app.something', navData: {title:'Test 1'}, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing', navData: {title:'Test 2'}, data: {public:false, roles:['whatever']}},
          ];
          var testController = createController(states);

          expect(testController.levelOneStates.length).to.equal(2);
        });

        it('when a level one state has showOnNav=false, dont count it as child',()=>{

          var states = [
            { name: 'app', navData: {title:'Test'}, data: {public:false, roles:['whatever']}},
            { name: 'app.something', navData: {title:'Test 1'}, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing', navData: {title:'Test 2', showOnNav:false}, data: {public:false, roles:['whatever']} },
          ];
          var testController = createController(states);

          expect(testController.levelOneStates.length).to.equal(1);
          expect(testController.levelOneStates[0].name).to.equal('app.something');
        });

        it('sets correctly a level two state when present',()=>{

          var states = [
            { name: 'app', navData: {title:'Test'}, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing', navData: {title:'Test 2'}, abstract:true, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing.child', navData: {title:'Test 3'}, url:'state', data: {public:false, roles:['whatever']}},
          ];
          var testController = createController(states);
          var levelOneState = testController.levelOneStates[0];

          expect(levelOneState.navData.childStates).not.to.be.undefined

        });
        it('when a level two state has parameters, dont count it as child',()=>{

          var states = [
            { name: 'app', navData: {title:'Test'}, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing', navData: {title:'Test 2'}, abstract:true, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing.child', navData: {title:'Test 3'}, url:'state/:id', data: {public:false, roles:['whatever']}},
          ];
          var testController = createController(states);
          var levelOneState = testController.levelOneStates[0];

          expect(levelOneState.navData.childStates).to.be.empty

        });
        it('when a level two state is listed as showOnNav=false, dont count it as child',()=>{

          var states = [
            { name: 'app', navData: {title:'Test'}, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing', navData: {title:'Test 2'}, abstract:true, data: {public:false, roles:['whatever']}},
            { name: 'app.otherthing.child', navData: {title:'Test 3', showOnNav:false}, url:'state', data: {public:false, roles:['whatever']}},
          ];
          var testController = createController(states);
          var levelOneState = testController.levelOneStates[0];

          expect(levelOneState.navData.childStates).to.be.empty

        });
      });
    })


  });
});
