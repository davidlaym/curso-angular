/// <reference path="../../appModule"/>
/// <reference path="../services/authPrincipalFac.ts"/>
/// <reference path="../../../typings/angularjs/angular.d.ts"/>
/// <reference path="../../../typings/angular-ui-router/angular-ui-router.d.ts"/>
/// <reference path="../../../typings/lodash/lodash.d.ts"/>

module app {
  export class NavDirectiveController
  {
    static directiveId = 'uirouterNav'
    static ddo : ng.IDirective = {
      restrict: 'E',
           templateUrl: '/templates/shared/directives/nav.tmpl.html',
           /*scope: {

           },*/
           controllerAs: 'vm',
           bindToController: true
    }

    rootState: app.INavState;
    levelOneStates: app.INavState[];

    static $inject = ['$state','app.authPrincipalFactory'];
    constructor(
      private $states:ng.ui.IStateService,
      private principal: app.AuthPrincipalFactoryClass) {

        var allStates = <app.INavState[]>$states.get();

        var allValidStates = _.filter(allStates, (state)=> state.name != "" );
        var rootState = _.find(allValidStates,(state)=> state.name == 'app');

        if(!rootState)
        {
          throw new Error('A root state was not provided. The root state must be named "app".');
        }

        this.rootState = rootState;

        this.levelOneStates = _.map(_.filter(allValidStates, (s)=> {return this.isValidLevelOneState(s); } ),(state)=> {
          if(state.abstract){
            state.navData.childStates = this.getLevelTwoStates(allStates, state);
          }
          return state;
        });

        //window.console.log('navDirective: rootState registered: ',this.rootState);
        //window.console.log('navDirective: leveloneState registered: ',this.levelOneStates);

    }

    private isValidLevelOneState(state) {
      var regex = new RegExp('app\.[a-z]+$');
      var preFilters = regex.test(state.name);
      return preFilters && this.runRouteFilters(state);
    }

    private getLevelTwoStates(allStates, levelOneState) {
      var regex = new RegExp(levelOneState.name+'\.[a-z]+');
      var childStates= _.filter(allStates,(state:any)=> {
        var preFilters = regex.test(state.name) && state.url.indexOf(":")==-1;
        return preFilters && this.runRouteFilters(state);
      });
      return childStates;
    }

    private runRouteFilters(state) {
      var isValid = true;
      isValid = isValid && this.isShownOnNav(state);
      isValid = isValid && this.isNotRestricted(state);
      return isValid;
    }

    private isShownOnNav(state) {
      return !state.navData || state.navData.showOnNav!==false;
    }

    private isNotRestricted(state) {
      if(state.data.public)
        return state.data.public;

      var allowed = this.principal.isInAnyRole(state.data.roles);
      return allowed;
    }
  }

  app.registerDirective(appModule, NavDirectiveController);
}
