/// <reference path="routingAnnotation.ts"/>
module app.shared.typings {

  export interface IContorllerAnotation {
    routing : IRoutingAnotation;
    controller: {
      id:string;
      templateUrl:string;

    }
  }
}
