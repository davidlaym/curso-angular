module app.shared.typings {
  export interface IRoutingAnotation {
    name:string;
    path:string;
    title:string;
    abstract?:boolean;
    showOnNav?:boolean;
    public?:boolean;
    roles?:string[];

  }

}
