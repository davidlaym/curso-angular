/// <reference path='../../../typings/angular-ui-router/angular-ui-router.d.ts'/>

declare module app {
  interface INavState extends ng.ui.IState {
    navData: {
      title:string;
      showOnNav?:boolean;
      childStates?: INavState[];
    };
    data: {
      public:boolean;
      roles?: string[];
    }
  }
}
