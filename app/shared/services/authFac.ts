/// <reference path="../../appModule"/>
/// <reference path="../../../typings/angularjs/angular.d.ts"/>
/// <reference path="./authPrincipalFac"/>
module app {
  export class AuthFactoryClass
  {
    static annotations = {
      name: 'app.authFactory'
    }

    static $inject = ['app.authPrincipalFactory', '$rootScope', '$state', '$q'];
    constructor(
      private principal: app.AuthPrincipalFactoryClass,
      private $rootScope: any,
      private $state: ng.ui.IStateService,
      private $q: ng.IQService) {

      return this;
    }

    authorize(): ng.IPromise<any> {
      if(this.$rootScope.toState === 'app.login') {
        var defered = this.$q.defer();
        defered.resolve(true);
        return defered.promise;

      } else {

      return this.principal.identity()
        .then(()=>{
           var isAuthenticated = this.principal.isAuthenticated();
           if (this.$rootScope.toState
              && this.$rootScope.toState.data
              && this.$rootScope.toState.data.roles
              && this.$rootScope.toState.data.roles.length > 0
              && !this.principal.isInAnyRole(this.$rootScope.toState.data.roles)) {

              if (isAuthenticated) this.$state.go('accessdenied'); // user is signed in but not authorized for desired state
              else {
                // user is not authenticated. stow the state they wanted before you
                // send them to the signin state, so you can return them when you're done
                this.$rootScope.returnToState = this.$rootScope.toState;
                this.$rootScope.returnToStateParams = this.$rootScope.toStateParams;

                // now, send them to the signin state so they can log in
                this.$state.go('app.login');
              }
            }
        },(err)=> {
          console.log(err);
          this.$state.go('app.login');
        });
      }
    }
  }

  app.registerFactory(appModule, AuthFactoryClass);
}
