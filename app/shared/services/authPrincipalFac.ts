/// <reference path="../../appModule"/>
/// <reference path="../../../typings/angularjs/angular.d.ts"/>

module app {
  export class AuthPrincipalFactoryClass
  {
    static annotations = {
      name: 'app.authPrincipalFactory'
    }

    private _authenticated: boolean;
    private _identity: any;

    static $inject = ['$q', 'dpd'];
    constructor(
      private $q: ng.IQService,
      private dpd) {

      return this;
    }
    isIdentityResolved() {
      return angular.isDefined(this._identity);
    }
    isAuthenticated() {
      return this._authenticated;
    }

    isInRole(role: string) {
      if (!this._authenticated || !this._identity.roles) return false;

      return this._identity.roles.indexOf(role) != -1;
    }

    isInAnyRole(roles: string[]) {
      if (!this._authenticated || !this._identity.roles) return false;

      for (var i = 0; i < roles.length; i++) {
        if (this.isInRole(roles[i])) return true;
      }

      return false;
    }
    authenticate(identity: any) {
      this._identity = identity;
      this._authenticated = identity != null;
    }

    identity(force?:boolean) {
      console.log('checkign identity');
      var deferred = this.$q.defer();

      if (force === true) this._identity = undefined;

      // check and see if we have retrieved the identity data from the server. if we have, reuse it by immediately resolving
      if (angular.isDefined(this._identity)) {
        console.log('using cached identity', this._identity);
        deferred.resolve(this._identity);
        return deferred.promise;
      }

      console.log('querying identity to api');
      this.dpd.people.get('me')
        .then((response)=> {
          console.log('response obtained', response);
          if(response.status != 200) {
            console.error('api returned http status not ok',response);
            deferred.reject(response.data);
          }
          else {
            this._identity = response.data;
            this._authenticated=true;
            console.log('idenity resolved', this._identity);
            deferred.resolve(this._identity);
          }
        },(err)=> {
          console.error('could not resolve identity',err);
          deferred.reject(err);
        });

      return deferred.promise;
    }
  }

  app.registerFactory(appModule, AuthPrincipalFactoryClass);
}
