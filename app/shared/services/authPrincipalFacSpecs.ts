/// <reference path='../../../typings/angularjs/angular.d.ts'/>
/// <reference path='../../../typings/angular-ui-router/angular-ui-router.d.ts'/>
/// <reference path='../../../typings/angularjs/angular-mocks.d.ts'/>
/// <reference path='../../../typings/mocha/mocha.d.ts'/>
/// <reference path='../../../typings/chai/chai.d.ts'/>
/// <reference path='../../../typings/sinon/sinon.d.ts'/>
/// <reference path='../typings/navState.d.ts'/>
/// <reference path='../../appModule'/>
/// <reference path='./authPrincipalFac'/>

describe('app',()=>{
  var expect = chai.expect;
  describe('authPrincipalFactoryClass', ()=> {

    function createController() {
      var $q = null;
      var dpd = null;

      return new app.AuthPrincipalFactoryClass($q, dpd);
    }


    it('factory should be defined', ()=> {
      expect(app.AuthPrincipalFactoryClass).not.to.be.null.and.not.to.be.undefined;
    });

    it('this is the failing test',()=>{
      expect(true).to.be.false;
    });

  });
});
