/// <reference path='../../../typings/angularjs/angular.d.ts'/>
/// <reference path='../../../typings/angular-ui-router/angular-ui-router.d.ts'/>
/// <reference path='../../../typings/angularjs/angular-mocks.d.ts'/>
/// <reference path='../../../typings/mocha/mocha.d.ts'/>
/// <reference path='../../../typings/chai/chai.d.ts'/>
/// <reference path='../../../typings/sinon/sinon.d.ts'/>
/// <reference path='../typings/navState.d.ts'/>
/// <reference path='../../appModule'/>
/// <reference path='./authFac'/>

describe('app',()=>{
  var expect = chai.expect;
  describe('authFactoryClass', ()=> {

    function createController() {
      var principal=null;
      var $rootScope=null;
      var $state=null;
      var $q=null;

      return new app.AuthFactoryClass(principal, $rootScope, $state, $q);
    }


    it('factory should be defined', ()=> {
      expect(app.AuthFactoryClass).not.to.be.null.and.not.to.be.undefined;
    });

    it('this is the failing test',()=>{
      expect(true).to.be.false;
    });

  });
});
