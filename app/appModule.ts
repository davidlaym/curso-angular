/// <reference path='shared/typings/controllerAnnotation'/>
/// <reference path='../typings/angular-ui-router/angular-ui-router.d.ts'/>

module app {
  export var appModule = angular.module('app',[
    'ui.router',
    'ui.bootstrap',
    'dpd',

    'app.people',
    'app.projects'
  ]);

  appModule.value('dpdConfig', {
    collections: ['tasks', 'projects','people'],
    serverRoot: 'http://localhost:2403', // optional, defaults to same server
    socketOptions: { reconnectionDelayMax: 3000 }, // optional socket io additional configuration
    useSocketIo: true, // optional, defaults to false
    noCache: true // optional, defaults to false (false means that caching is enabled, true means it disabled)
  });



  export function registerController(parentModule: ng.IModule, controller) {

    var stateConfig = <INavState>{
      url: controller.annotations.routing.path,
      controller: controller.annotations.controller.id,
      controllerAs: 'vm',
      abstract: controller.annotations.routing.abstract,
      templateUrl: controller.annotations.controller.templateUrl,
      navData: {
        title: controller.annotations.routing.title,
        showOnNav: controller.annotations.routing.showOnNav
      },
      data: {
        public:false
      }
    };

    if(!controller.annotations.routing.public) {
      stateConfig.resolve = {
        authorize: ['app.authFactory',
          (auth: app.AuthFactoryClass)=> {
            return auth.authorize();
          }
        ]
      };

      if(controller.annotations.routing.roles) {
        stateConfig.data = {
          roles: controller.annotations.routing.roles,
          public:false
        }
      }
    } else {
      stateConfig.data.public= true;
    }


    parentModule
        .controller(controller.annotations.controller.id, controller);
        parentModule
          .config([
            '$stateProvider',
            ($stateProvider: ng.ui.IStateProvider) => {
                $stateProvider
                    .state(controller.annotations.routing.name, stateConfig);
            }]);
  }
  export function registerDirective(parentModule: ng.IModule, directive) {
    var factory: ng.IDirectiveFactory = () => {
      var ddo =directive.ddo;

      ddo.controller = directive;
      return ddo;
    };
    parentModule
      .directive(directive.directiveId, factory);
  }

  export function registerFactory(parentModule: ng.IModule, factoryClass) {
    var name = factoryClass.annotations.name;

    var factoryDef = function() {

      // this is some serious js kung-fu learned in http://stackoverflow.com/a/18240186
      // basically allows to new-up a object passing variable constructor arguments
      // and allows me to define a factory with a simple class instead of
      // a nested function of evil

      var args = Array.prototype.concat.apply([null], arguments);
      return new (Function.prototype.bind.apply(factoryClass, args));
    };

    factoryDef.$inject = factoryClass.$inject;
    parentModule.factory(name,factoryDef);
  }
}
