/// <reference path='../../typings/angularjs/angular.d.ts'/>
/// <reference path='../appModule'/>
/// <reference path='../appCtrl'/>

module app.people {
  export var peopleModule = angular.module('app.people',[
    // 3rd party deps
    'ui.router',
    'dpd',
    'angular-md5'
    ]);

}
