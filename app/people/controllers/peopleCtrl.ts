/// <reference path='../peopleModule'/>

module app.people {
  export class PeopleController {
    static annotations: app.shared.typings.IContorllerAnotation = {
      routing: {
        name:'app.people',
        path:'/people',
        title:'People',
        abstract:true
      },
      controller: {
        id:'app.people.PeopleController',
        templateUrl:'templates/people/controllers/people.tmpl.html',
      }
    }
    static $inject = ['dpd'];
    constructor(dpd) {

    }
  }
  app.registerController(peopleModule, PeopleController);
}
