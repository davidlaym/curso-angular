/// <reference path="../peopleModule"/>
module app.people {
  export class PeopleDetailsController {
    static annotations: app.shared.typings.IContorllerAnotation = {
      routing: {
        name:'app.people.details',
        path:'/detail/:id',
        title:'Person Details'
      },
      controller: {
        id:'app.people.PeopleDetailsController',
        templateUrl:'templates/people/controllers/peopleDetails.tmpl.html',
      }
    }
    static $inject = ['$state', 'dpd'];

    person:any;
    constructor($state:ng.ui.IStateService, dpd) {
      var params:any = $state.params;
      dpd.people.get({id: params.id}).then((response)=> {
        this.person = response.data;
      },(err)=> {
        window.console.error(err);
      });

    }
  }
  app.registerController(peopleModule, PeopleDetailsController);
}
