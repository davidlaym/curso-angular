module app.people {
  export class PeopleListController {
    static annotations: app.shared.typings.IContorllerAnotation = {
      routing: {
        name:'app.people.list',
        path:'/list',
        title:'People List'
      },
      controller: {
        id:'app.people.PeopleListController',
        templateUrl:'templates/people/controllers/peopleList.tmpl.html',
      }
    }
    static $inject = ['dpd'];

    people:any[];
    constructor(dpd) {
      dpd.people.get().then((result)=> {
        this.people = result.data;
      },(err)=> {
        window.console.error(err);
      });
    }
  }
  app.registerController(peopleModule, PeopleListController);
}
