# Folder structure

```
	/
	-app/
	  -moduleName/
	    -controllers/
        -someCtrl.ts      // controller route def. and implementation
        -someCtrl.less    // styles for the template of the controller
        -someCtrl.html    // template markup for the controller
        -someCtrlSpec.ts  // tests for the controller
	    -services/
        -someSrv.ts       // service implementation and registration
        -someSrvSpec.ts   // tests for the service
	    -directives/
        -someDir.ts       // directive def. and implementation
        -someDir.less     // styles for the template of the directive
        -someDir.html     // template markup for the directive
        -someDirSpec.ts   // tests for the controller
	    -module.js          // module definition and configuration
      -styles/            // module-level styles
        -modlule.less     // entry point for the module (imports)
        -someStyles.less  // styles can be separated in multiple files

	  -shared/              // is good idea to reserve a special shared module
	    - ...               // folders are same as other modules
    -assets/
      -fonts/             // local fonts in use
      -img/               // static images for styles
      -app.less           // app-level styles and imports from modules
    -app.ts               // application bootstrapping
	-wwwroot/               // folder of compiled code ready for prod.
  -node_modules/
  -bower_components/
  -bower.json
  -gulpfile.js
  -package.json
```
